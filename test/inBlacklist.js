var chai = require('chai');
var expect = chai.expect;
var validator = require('validator');

var inBlacklist = require('../sources/inBlacklist');

describe('URL Validation : inBlacklist', function() {

    it('Must be true', function() {
        var result = inBlacklist();
        expect(result).to.be.true;
    });

});