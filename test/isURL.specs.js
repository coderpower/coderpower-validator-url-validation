var chai = require('chai');
var expect = chai.expect;
var validator = require('validator');

var isURL = require('../sources/isURL');

describe('URL Validation : isURL', function() {

    it('Must be true', function() {
        var result = isURL();
        expect(result).to.be.true;
    });

});