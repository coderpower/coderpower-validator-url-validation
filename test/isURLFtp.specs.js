var chai = require('chai');
var expect = chai.expect;
var validator = require('validator');

var isURLFtp = require('../sources/isURLFtp');

describe('String Validation : URLWithFTP', function() {

    it('Must be true', function() {
        var result = isURLFtp();
        expect(result).to.be.false;
    });

});