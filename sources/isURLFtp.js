var validator = require('validator');

module.exports = function URLWithFTP() {
    var input = 'http://foobar.com';

    var validated = validator.isURL(input, {protocols: ['ftp']});
    console.log('validated', validated);

    return validated;
};