var validator = require('validator');

module.exports = function isURL() {
    var input = 'http://foobar.com';

    var validated = validator.isURL(input);
    console.log('validated', validated);

    return validated;
};