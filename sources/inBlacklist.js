var validator = require('validator');

module.exports = function inBlacklist() {
    var input = 'http://foobar.com';

    var validated = validator.isURL(input, {host_blacklist: ['foo.com', 'bar.com']});
    console.log('validated', validated);

    validated = validator.isURL(input, {host_blacklist: ['foo.com']});
    console.log('validated', validated);

    return validated;
};