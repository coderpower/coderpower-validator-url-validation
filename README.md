### Url Validation

In this example we will cover the following function:

```javascript
    var isURL = validator.isURL('http://foobar.com');
```

For more information, please refer to the documentation: https://github.com/chriso/validator.js#readme
